.. :changelog:

History
-------

1.0.2 (2020-05-21)
++++++++++++++++++

* update dependencies


1.0.1 (2019-01-30)
++++++++++++++++++

* SQL views for kamailio
* translations updates

1.0.0 (2018-11-15)
++++++++++++++++++

* First release on PyPI.
