# -*- coding: utf-8
from django.apps import AppConfig


class PyfbNormalizationConfig(AppConfig):
    name = 'pyfb_normalization'
