============
Installation
============

At the command line::

    $ easy_install pyfb-normalization

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv pyfb-normalization
    $ pip install pyfb-normalization
