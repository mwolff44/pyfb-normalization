=====
Usage
=====

To use pyfb-normalization in a project, add it to your `INSTALLED_APPS`:

.. code-block:: python

    INSTALLED_APPS = (
        ...
        'pyfb_normalization.apps.PyfbNormalizationConfig',
        ...
    )

Add pyfb-normalization's URL patterns:

.. code-block:: python

    from pyfb_normalization import urls as pyfb_normalization_urls


    urlpatterns = [
        ...
        url(r'^', include(pyfb_normalization_urls)),
        ...
    ]
