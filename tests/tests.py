#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase
from django.urls import reverse
from django.test import Client

from pyfb_normalization.models import CallMappingRule, NormalizationGrp, NormalizationRule, NormalizationRuleGrp


def create_callmappingrule(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["dpid"] = 1
    defaults["pr"] = 1
    defaults["match_op"] = 1
    defaults["match_exp"] = "match_exp"
    defaults["match_len"] = 10
    defaults["subst_exp"] = "subst_exp"
    defaults["repl_exp"] = "repl_exp"
    defaults["attrs"] = "PSTN"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return CallMappingRule.objects.create(**defaults)


def create_normalizationgrp(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return NormalizationGrp.objects.create(**defaults)


def create_normalizationrule(**kwargs):
    defaults = {}
    defaults["name"] = "name"
    defaults["match_op"] = 1
    defaults["match_exp"] = "match_exp"
    defaults["match_len"] = 10
    defaults["subst_exp"] = "subst_exp"
    defaults["repl_exp"] = "repl_exp"
    defaults["attrs"] = ""
    defaults["description"] = "description"
    defaults.update(**kwargs)
    return NormalizationRule.objects.create(**defaults)


def create_normalizationrulegrp(**kwargs):
    defaults = {}
    defaults["pr"] = 1
    defaults["description"] = "description"
    defaults.update(**kwargs)
    if "dpid" not in defaults:
        defaults["dpid"] = create_normalizationgrp()
    if "rule" not in defaults:
        defaults["rule"] = create_normalizationrule()
    return NormalizationRuleGrp.objects.create(**defaults)


class CallMappingRuleViewTest(TestCase):
    '''
    Tests for CallMappingRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_callmappingrule(self):
        url = reverse('pyfb-normalization:pyfb_normalization_callmappingrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_callmappingrule(self):
        url = reverse('pyfb-normalization:pyfb_normalization_callmappingrule_create')
        data = {
            "name": "name",
            "dpid": 1,
            "pr": 1,
            "match_op": 1,
            "match_exp": "match_exp",
            "match_len": 10,
            "subst_exp": "subst_exp",
            "repl_exp": "repl_exp",
            "attrs": "PSTN",
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_callmappingrule(self):
        callmappingrule = create_callmappingrule()
        url = reverse('pyfb-normalization:pyfb_normalization_callmappingrule_detail', args=[callmappingrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_callmappingrule(self):
        callmappingrule = create_callmappingrule()
        data = {
            "name": "name",
            "dpid": 1,
            "pr": 1,
            "match_op": 1,
            "match_exp": "match_exp",
            "match_len": 10,
            "subst_exp": "subst_exp",
            "repl_exp": "repl_exp",
            "attrs": "PSTN",
            "description": "description",
        }
        url = reverse('pyfb-normalization:pyfb_normalization_callmappingrule_update', args=[callmappingrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class NormalizationGrpViewTest(TestCase):
    '''
    Tests for NormalizationGrp
    '''
    def setUp(self):
        self.client = Client()

    def test_list_normalizationgrp(self):
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationgrp_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_normalizationgrp(self):
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationgrp_create')
        data = {
            "name": "name",
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_normalizationgrp(self):
        normalizationgrp = create_normalizationgrp()
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationgrp_detail', args=[normalizationgrp.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_normalizationgrp(self):
        normalizationgrp = create_normalizationgrp()
        data = {
            "name": "name",
            "description": "description",
        }
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationgrp_update', args=[normalizationgrp.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class NormalizationRuleViewTest(TestCase):
    '''
    Tests for NormalizationRule
    '''
    def setUp(self):
        self.client = Client()

    def test_list_normalizationrule(self):
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrule_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_normalizationrule(self):
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrule_create')
        data = {
            "name": "name",
            "match_op": 1,
            "match_exp": "match_exp",
            "match_len": 10,
            "subst_exp": "subst_exp",
            "repl_exp": "repl_exp",
            "attrs": "",
            "description": "description",
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_normalizationrule(self):
        normalizationrule = create_normalizationrule()
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrule_detail', args=[normalizationrule.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_normalizationrule(self):
        normalizationrule = create_normalizationrule()
        data = {
            "name": "name",
            "match_op": 1,
            "match_exp": "match_exp",
            "match_len": 10,
            "subst_exp": "subst_exp",
            "repl_exp": "repl_exp",
            "attrs": "",
            "description": "description",
        }
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrule_update', args=[normalizationrule.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)


class NormalizationRuleGrpViewTest(TestCase):
    '''
    Tests for NormalizationRuleGrp
    '''
    def setUp(self):
        self.client = Client()

    def test_list_normalizationrulegrp(self):
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrulegrp_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_create_normalizationrulegrp(self):
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrulegrp_create')
        data = {
            "pr": 1,
            "description": "description",
            "dpid": create_normalizationgrp().pk,
            "rule": create_normalizationrule().pk,
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 302)

    def test_detail_normalizationrulegrp(self):
        normalizationrulegrp = create_normalizationrulegrp()
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrulegrp_detail', args=[normalizationrulegrp.pk,])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_update_normalizationrulegrp(self):
        normalizationrulegrp = create_normalizationrulegrp()
        data = {
            "pr": 1,
            "description": "description",
            "dpid": create_normalizationgrp().pk,
            "rule": create_normalizationrule().pk,
        }
        url = reverse('pyfb-normalization:pyfb_normalization_normalizationrulegrp_update', args=[normalizationrulegrp.pk,])
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, 302)
